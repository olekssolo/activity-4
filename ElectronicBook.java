public class ElectronicBook extends Book {
    int numberBytes;

    public ElectronicBook(String book, String author, int numberBytes) {
    super(book, author);
    this.numberBytes = numberBytes;
    }

    public int getNumberBytes() {
        return this.numberBytes;
    }
    public String toString() {
        String base = super.toString();
        base = base + ", " + this.numberBytes;
        return base;
    }
}
