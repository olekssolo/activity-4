public class BookStore {
    public static void main(String[] args) {
        Book[] books = new Book[5];
        books[0] = new Book("Holes", "Louis Sachar");
        books[1] = new ElectronicBook("Philosopher's Stone", "J. K. Rowling", 24);
        books[2] = new Book("Into the Wild", "Erin Hunter");
        books[3] = new ElectronicBook("Eclipse", "Erin Hunter", 20);
        books[4] = new ElectronicBook("The Darkest Hour", "Erin Hunter", 2);

        for (int i=0; i<books.length; i++) {
            System.out.println(books[i].toString());
        }
    }
}
